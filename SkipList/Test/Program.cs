﻿using SkipList;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Test
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Random random = new Random();
            SkipList<int, int> list = new SkipList<int, int>(Enumerable.Range(0, 100).Select(x => new KeyValuePair<int, int>(x, x)).OrderBy(x => random.NextDouble()));
            foreach (var item in list.Keys)
                Console.WriteLine(item);
            list.Remove(0);
            foreach (var item in list.Keys)
                Console.WriteLine(item);
            Console.WriteLine(list.ContainsKey(14));
            Console.WriteLine();
        }
    }
}
