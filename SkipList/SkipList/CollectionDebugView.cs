﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace SkipList
{
    /// <summary>
    /// Determines how a collection is viewed in the Visual Studio debugger.
    /// </summary>
    /// <typeparam name="T">The value type.</typeparam>
    internal sealed class CollectionDebugView<T>
    {
        /// <summary>
        /// The collection items.
        /// </summary>
        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        public T[] Items
        {
            get
            {
                T[] array = new T[source.Count];
                source.CopyTo(array, 0);
                return array;
            }
        }

        /// <summary>
        /// Constructs the new instance of CollectionDebugView with the specified collection.
        /// </summary>
        /// <param name="collection">The collection.</param>
        public CollectionDebugView(ICollection<T> collection)
        {
            if (collection is null)
                throw new ArgumentNullException(nameof(collection));

            source = collection;
        }

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private readonly ICollection<T> source;
    }
}
