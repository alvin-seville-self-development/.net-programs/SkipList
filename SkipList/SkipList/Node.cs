﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;

namespace SkipList
{
    /// <summary>
    /// Represents a skip list node.
    /// </summary>
    /// <typeparam name="TKey">The key type.</typeparam>
    /// <typeparam name="TValue">The value type.</typeparam>
    [Serializable]
    [DebuggerDisplay("Key = {Key}, Value = {Value}")]
    public class Node<TKey, TValue> : IEquatable<Node<TKey, TValue>>,
        ICloneable,
        IFormattable
    {
        /// <summary>
        /// The key.
        /// </summary>
        public TKey Key { get; }

        /// <summary>
        /// The value.
        /// </summary>
        public TValue Value { get; internal set; }

        /// <summary>
        /// The level.
        /// </summary>
        public int Level { get; }

        /// <summary>
        /// Retrieves or sets the particular forward node.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns>The item by the specified index.</returns>
        public Node<TKey, TValue> this[int index]
        {
            get => forwardNodes[index];
            set => forwardNodes[index] = value;
        }

        /// <summary>
        /// Constructs the new instance of Node.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <param name="level">The level</param>
        public Node(TKey key, TValue value, int level)
        {
            if (ReferenceEquals(key, null))
                throw new ArgumentNullException(nameof(key));
            if (level < 0)
                throw new ArgumentOutOfRangeException(nameof(level));

            Key = key;
            Value = value;
            Level = level;
            forwardNodes = new Node<TKey, TValue>[++level];
        }

        /// <summary>
        /// Deconstructs the current instance of Node into two out-parameters.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public void Deconstruct(out TKey key, out TValue value)
        {
            key = Key;
            value = Value;
        }

        /// <summary>
        /// Evaluates whether to instances of Node are equal.
        /// </summary>
        /// <param name="other">The second instance.</param>
        /// <returns>The evaluation result.</returns>
        public bool Equals(Node<TKey, TValue> other)
        {
            if (Level != other.Level)
                return false;
            for (int i = 0; i < Level; i++)
                if (!ReferenceEquals(forwardNodes[i], other[i]))
                    return false;
            return Value.Equals(other.Value) && Key.Equals(other.Key);
        }

        /// <summary>
        /// Evaluates whether to instances of Node are equal.
        /// </summary>
        /// <param name="other">The second instance.</param>
        /// <returns>The evaluation result.</returns>
        public override bool Equals(object obj)
        {
            Node<TKey, TValue> node = obj as Node<TKey, TValue>;
            if (node is null)
                throw new ArgumentException(nameof(obj));
            return base.Equals(node);
        }

        /// <summary>
        /// Returns the clone of the current instance of Node.
        /// </summary>
        /// <returns>The instance clone.</returns>
        public Node<TKey, TValue> Clone()
        {
            Node<TKey, TValue> clone = new Node<TKey, TValue>(Key, Value, Level);
            for (int i = 0; i < forwardNodes.Length; i++)
                clone[i] = forwardNodes[i];
            return clone;
        }

        /// <summary>
        /// Returns the clone of the current instance of Node.
        /// </summary>
        /// <returns>The instance clone.</returns>
        object ICloneable.Clone()
        {
            return Clone();
        }

        /// <summary>
        /// Returns the string representation of the current object.
        /// </summary>
        /// <returns>The string representation of the current object.</returns>
        public override string ToString()
        {
            return ToString(null, null);
        }

        /// <summary>
        /// Returns the string representation of the current object.
        /// </summary>
        /// <param name="format">The format string.</param>
        /// <param name="formatProvider">The object, that provides some format settings.</param>
        /// <returns>The string representation of the current object.</returns>
        public string ToString(string format, IFormatProvider formatProvider)
        {
            format = format ?? DefaultFormat;
            formatProvider = DefaultFormatProvider;
            return string.Format(formatProvider, format, Key);
        }

        /// <summary>
        /// Returns the object hash code.
        /// </summary>
        /// <returns>The object hash code.</returns>
        public override int GetHashCode()
        {
            var hashCode = 1425366339;
            hashCode = hashCode * -1521134295 + EqualityComparer<TKey>.Default.GetHashCode(Key);
            hashCode = hashCode * -1521134295 + EqualityComparer<TValue>.Default.GetHashCode(Value);
            hashCode = hashCode * -1521134295 + Level.GetHashCode();
            return hashCode;
        }

        public static bool operator ==(Node<TKey, TValue> left, Node<TKey, TValue> right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Node<TKey, TValue> left, Node<TKey, TValue> right)
        {
            return !(left == right);
        }

        private const string DefaultFormat = "{0}";

        [NonSerialized]
        private readonly IFormatProvider DefaultFormatProvider = CultureInfo.CurrentCulture;
        private readonly Node<TKey, TValue>[] forwardNodes;
    }
}
